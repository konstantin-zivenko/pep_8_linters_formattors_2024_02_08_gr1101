import numbers


def clean_code(code):
    ...


user_name = "Petro"


class ShopUser:
    def __init__(self):
        self.name = user_name

    def __str__(self):
        return str(self.__dict__)


DATA_FILE_PATH = "data"


# package, pythonpackage
x = "John Smith"
y, z = x.split()
print(f"{z}, {y}")
full_name = "John Smith"
first_name, last_name = full_name.split()

print(f"{last_name}, {first_name}")


def db(x):
    return x * 2


def multiply_by_two(x: numbers) -> numbers:
    """
    docstring
    """
    return x * 2


# some comment
def function(arg_one, arg_two, arg_three, arg_four, arg_five):
    # some block comment
    return arg_one  # some comment


list_of_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
