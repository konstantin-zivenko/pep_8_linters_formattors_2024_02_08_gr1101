def special_revers(text: str) -> str:
    return text[::-1]


if __name__ == "__main__":
    cases = (
        ("asdf", "fdsa"),
        ("as#5df", "fd#5sa"),
        ("asdf qwer", "fdsa rewq"),
        ("zxc567op qw123rt7", "poc567xz tr123wq7"),
    )

    for arg, result in cases:
        assert (
            special_revers(arg) == result
        ), f"ERROR: special_revers({arg}) returned {special_revers(arg)}, but expected: {result} "
